#!/bin/bash
firewall-cmd --zone=public --add-port=7000/tcp --permanent
firewall-cmd --zone=public --add-port=7001/tcp --permanent
firewall-cmd --zone=public --add-port=7002/tcp --permanent
firewall-cmd --zone=public --add-port=7003/tcp --permanent
firewall-cmd --zone=public --add-port=7004/tcp --permanent
firewall-cmd --zone=public --add-port=7005/tcp --permanent
firewall-cmd --zone=public --add-port=7006/tcp --permanent
firewall-cmd --zone=public --add-port=7007/tcp --permanent
firewall-cmd --zone=public --add-port=7008/tcp --permanent
firewall-cmd --zone=public --add-port=7009/tcp --permanent
firewall-cmd --zone=public --add-port=7010/tcp --permanent
firewall-cmd --reload